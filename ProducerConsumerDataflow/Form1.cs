﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace ProducerConsumerDataflow
{

    public partial class Form1 : Form
    {
        private DataFlow _flow;

        public Form1()
        {
            _flow = new DataFlow(this);
            InitializeComponent();

        }

        public void AddItemsToListBox(IEnumerable<string> s)
        {
            if (this.listBox1.InvokeRequired)
            {
                listBox1?.Invoke(new Action (() => { listBox1.Items.AddRange(s.ToArray<string>()); }));
            }
            else
            {
                listBox1.Items.AddRange(s.ToArray<string>());
            }
            
        }

        public void AddItemsToListBox(string s)
        {
            if (this.listBox1.InvokeRequired)
            {
                listBox1?.Invoke(new Func<int>(() => { return listBox1.Items.Add(s); }));
            }
            else
            {
                listBox1.Items.Add(s);
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add("Loading...");
            ConfigureButtonsForRun(true);
            await _flow.Start().ContinueWith(x => {
                if (this.InvokeRequired)
                {
                    this.Invoke(new Action(() => { ConfigureButtonsForRun(false); }));
                }
                else
                {
                    ConfigureButtonsForRun(false);
                }
            });
        }

        private void ConfigureButtonsForRun(bool running)
        {
            if (running)
            {
                button1.Enabled = false;
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
                button1.Enabled = true;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
             _flow.Cancel();
        }
    }
}
