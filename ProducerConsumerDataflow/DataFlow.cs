﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace ProducerConsumerDataflow
{
    public class DataFlow
    {
        BufferBlock<string> _messageQueue;
        readonly string DONE = "Done.";
        private Task _producer;
        private Task _consumer;
        private Form1 _view;
        CancellationTokenSource _cancellationSource;

        public DataFlow(Form1 view)
        {
            _view = view;
        }

        public async Task Start()
        {
            Stopwatch _sw = new Stopwatch();
            _sw.Restart();

            try
            {
                _sw = await CreatePipeline();
                _view.AddItemsToListBox(string.Format("Done! Processing took {0} seconds. ", _sw.Elapsed));
            }
            catch (OperationCanceledException)
            {
                _sw.Stop();
                _view.AddItemsToListBox(string.Format("Operation Canceled. Processing took {0} seconds.", _sw.Elapsed));
            }
        }

        public void Cancel()
        {
            _cancellationSource.Cancel();

        }

        private async Task<Stopwatch> CreatePipeline()
        {
            // for handling cancellation requests from the user
            GenerateNewCancellationSource();
            CancellationToken ct = _cancellationSource.Token;

            _messageQueue = new BufferBlock<string>(new DataflowBlockOptions { CancellationToken = ct });
            // stopwatch object added only because I was curious to see runtime
            Stopwatch _sw = new Stopwatch();

            _sw.Restart();

            _producer = Task.Run(() => ProducerThread(_messageQueue, ct));
            _consumer = Task.Run(() => QueueReceiveMessageRequest(_messageQueue, ct));
            await Task.WhenAll(_producer, _consumer);

            _sw.Stop();

            return _sw;
        }

        async Task QueueReceiveMessageRequest(ISourceBlock<string> source, CancellationToken c)
        {
            try
            {
                c.ThrowIfCancellationRequested();

                await Task.Delay(500);
                string message = await _messageQueue.ReceiveAsync(c);

                _view.AddItemsToListBox(string.Format("Processing message: {0}", message));

                if (!string.IsNullOrEmpty(message))
                {
                    _view.AddItemsToListBox(string.Format(string.Format("Received Message: {0}", message)));
                }

                if (DONE.Equals(message))
                {
                    // added to pause program so message output can be looked at
                    await Task.Delay(5000);
                }
                else
                {
                    await QueueReceiveMessageRequest(source, c);
                }

            }

            catch (OperationCanceledException)
            {
                Console.WriteLine("Canceling rx request.");
            }
        }

        private async Task ProducerThread(ITargetBlock<string> target, CancellationToken c)
        {
            int sleeptime = 100;

            for (int i = 0; i < 10; ++i)
            {
                c.ThrowIfCancellationRequested();

                string message = string.Format("Message #{0}", i);
                await target.SendAsync(message);
                _view.AddItemsToListBox(string.Format("Sent message: {0}", message));
                
                await Task.Delay(sleeptime);
                sleeptime += 100;
            }

            c.ThrowIfCancellationRequested();
            _view.AddItemsToListBox(DONE);
            await target.SendAsync(DONE);
        }

        private void GenerateNewCancellationSource()
        {
            // useful when new pipeline is created! otherwise old token remains
            _cancellationSource = new CancellationTokenSource();
        }
    }
}
